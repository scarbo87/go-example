# Go example
============

Краткое описание.
При старте демон:

1. Регистрирует обработчик "мягкого" выключения

1. Инициализирует логгер (используется logrus)

1. Выполняет initial select в БД для получения активных сущностей, сохраняет их в своем внутреннем кэше

1. Подписывается на mysql binlog репликацию, по которой получает все обновления из БД

1. На 81 порту подымает сервер для снятия метрик (expvar)

1. На 80 порту подымает основной сервер для обработки пользовательских запросов


#### Проверено на:

1. docker version 18.06.0-ce

1. docker-compose version 1.22.0


#### Запуск в докере:

1. docker-compose up


#### Роуты:

1. http://0.0.0.0:81/debug/vars - вывод expvar с кастомными полями

1. http://0.0.0.0:80/cache?name=Example.Room - json вывод памяти демона для таблички Example.Room (для env=prod закрыт Basic Auth)

1. http://0.0.0.0:80/cache?name=Example.User - json вывод памяти демона для таблички Example.User (для env=prod закрыт Basic Auth) (осторожно, много записей)

1. http://0.0.0.0:80/rooms/:id - json вывод сущности Room по ее id

1. http://0.0.0.0:80/users/:id - json вывод сущности User по ее id


#### Мониторинг:

Для разработчика реализован базовый консольный мониторинг использующий expvarmon (prometheus был бы излишен)

```
go get github.com/divan/expvarmon

expvarmon -ports="81" -vars="str:Version,Errors,Warnings,Goroutines,duration:Uptime,mem:memstats.Alloc,str:memstats.EnableGC,Binlog.Position,Heartbeat"
```
