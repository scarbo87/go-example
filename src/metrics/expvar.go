package metrics

import (
	"runtime"
	"time"
	"expvar"

	"github.com/siddontang/go-mysql/canal"
)

var (
	Version   = expvar.NewString("Version")
	Errors    = expvar.NewInt("Errors")
	Warnings  = expvar.NewInt("Warnings")
	Heartbeat = expvar.NewInt("Heartbeat")
	Canal     *canal.Canal

	startTime = time.Now().UTC()
)

func init() {
	expvar.Publish("Goroutines", expvar.Func(func() interface{} {
		return runtime.NumGoroutine()
	}))
	expvar.Publish("Uptime", expvar.Func(func() interface{} {
		return int64(time.Since(startTime))
	}))
	expvar.Publish("Binlog", expvar.Func(func() interface{} {
		binlog := struct {
			Position uint32
			File     string
		}{}

		if Canal != nil {
			binlog.Position = Canal.SyncedPosition().Pos
			binlog.File = Canal.SyncedPosition().Name
		}
		return binlog
	}))
}
