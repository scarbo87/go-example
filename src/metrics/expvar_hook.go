package metrics

import "github.com/sirupsen/logrus"

type ExpvarHook struct {
}

//Return slice of logrus levels with witch hook work
func (h *ExpvarHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.WarnLevel,
	}
}

//Execute hook
func (h *ExpvarHook) Fire(entry *logrus.Entry) error {
	switch entry.Level {
	case logrus.PanicLevel:
		Errors.Add(1)
	case logrus.FatalLevel:
		Errors.Add(1)
	case logrus.ErrorLevel:
		Errors.Add(1)
	case logrus.WarnLevel:
		Warnings.Add(1)
	}

	return nil
}
