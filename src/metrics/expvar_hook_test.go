package metrics

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/sirupsen/logrus"
)

func TestExpvarHook_Fire(t *testing.T) {
	testCases := []struct {
		entry    *logrus.Entry
		warnings int64
		errors   int64
	}{
		{&logrus.Entry{Level: logrus.PanicLevel}, 0, 1},
		{&logrus.Entry{Level: logrus.FatalLevel}, 0, 1},
		{&logrus.Entry{Level: logrus.ErrorLevel}, 0, 1},
		{&logrus.Entry{Level: logrus.WarnLevel}, 1, 0},
		{&logrus.Entry{Level: logrus.InfoLevel}, 0, 0},
	}

	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			Warnings.Set(0)
			Errors.Set(0)
			hook := &ExpvarHook{}


			hook.Fire(tc.entry)
			assert.Equal(t, tc.warnings, Warnings.Value())
			assert.Equal(t, tc.errors, Errors.Value())
		})
	}
}

func TestExpvarHook_Levels(t *testing.T) {
	hook := &ExpvarHook{}
	assert.Equal(t, []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.WarnLevel,
	}, hook.Levels())
}
