package handler

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	response "go-example/src/http-response"
	"go-example/src/model"
)

func GetCache(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var value interface{}
	tableName := r.URL.Query().Get("name")
	if val, ok := model.RepPull[tableName]; ok {
		value = val.GetCache()
	}

	response.JSON().SuccessResult(value).Render(w)
}
