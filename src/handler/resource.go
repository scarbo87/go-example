package handler

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"strconv"
	"go-example/src/model"
	response "go-example/src/http-response"
)

func GetUser(w http.ResponseWriter, _ *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		response.JSON().BadRequestError().Render(w)
		return
	}

	user, ok := model.UserRep.FindInCache(id).(model.User)
	if !ok {
		response.JSON().NotFoundError().Render(w)
		return
	}

	response.JSON().SuccessResult(user).Render(w)
}

func GetRoom(w http.ResponseWriter, _ *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		response.JSON().BadRequestError().Render(w)
		return
	}

	room, ok := model.RoomRep.FindInCache(id).(model.Room)
	if !ok {
		response.JSON().NotFoundError().Render(w)
		return
	}

	response.JSON().SuccessResult(room).Render(w)
}
