package repository

import "testing"

// Тест покрывает инициализацию объекта и метод GetCache
func TestBaseSliceDbModelInit(t *testing.T) {
	v := RepositorySliced{}

	v.GetCache()
	if !v.isInit {
		t.Error("Base Slice Model should be inited before GetCache")
	}
}

//Тест покрывает инициализацию объекта и методы FindInCache, AddToCache
func TestBaseSlicedDbModelFindInCache(t *testing.T) {
	testCases := []testModelSliced{
		{1, true, 1},
		{2, false, 1},
		{3, true, 2},
		{4, false, 1},
		{5, true, 2},
		{6, false, 1},
		{7, true, 2},
		{8, false, 1},
		{9, false, 2},
		{10, false, 1},
	}
	v := RepositorySliced{}

	res := v.FindInCache(1)
	if !v.isInit {
		t.Error("Base Slice Model should be inited before FindInCache")
	}

	if res != nil {
		t.Error("Not empty result")
	}
	for _, tc := range testCases {
		v.AddToCache(testModelSliced{tc.relationKey, tc.active, tc.id})
		if !v.isInit {
			t.Error("Base Slice Model should be inited before AddToCache")
		}
		res = v.FindInCache(tc.id)
		if res == nil {
			t.Error("Empty result after put")
		}
	}

}

//Тест покрывает инициализацию объекта и методы FindInCache, AddToCache, ClearCache
func TestBaseSlicedDbModelClearCache(t *testing.T) {
	v := RepositorySliced{}

	v.AddToCache(testModelSliced{1, true, 1})
	if !v.isInit {
		t.Error("Base Slice Model should be inited before AddToCache")
	}
	res := v.FindInCache(1)
	if res == nil {
		t.Error("Empty result after put")
	}
	res = v.FindInCache(2)
	if res != nil {
		t.Error("Found bad result")
	}

	v.ClearCache()
	if len(v.cache) != 0 {
		t.Error("Not empty result after clear")
	}
}

//Проверка работы функции Len()
func TestBaseSlicedDbModelLen(t *testing.T) {
	v := RepositorySliced{}

	v.AddToCache(testModelSliced{1, true, 1})
	v.AddToCache(testModelSliced{2, true, 2})
	v.AddToCache(testModelSliced{3, true, 3})
	if !v.isInit {
		t.Error("Base Slice Model should be inited before AddToCache")
	}
	if 3 != v.Len() {
		t.Error("Len function dont't show actual length")
	}
	v.AddToCache(testModelSliced{4, true, 4})
	v.AddToCache(testModelSliced{3, true, 3})
	if 4 != v.Len() {
		t.Error("Len function dont't show actual length")
	}
}

//Проверка добавления двух значений с одинаковым relationKey
func TestBaseSlicedDbModelAddToCacheDuplicate(t *testing.T) {
	testCases := []testModelSliced{
		{1, true, 1},
		{1, true, 1},
	}
	v := RepositorySliced{}

	for _, tc := range testCases {
		v.AddToCache(testModelSliced{tc.relationKey, tc.active, tc.id})
	}
	if len(v.cache) != 1 {
		t.Error("Remove of duplicates crash")
	}
}

type testModelSliced struct {
	relationKey int
	active      bool
	id          int
}

func (m testModelSliced) GetRelationKey() int { return m.relationKey }
func (m testModelSliced) IsActive() bool      { return m.active }
func (m testModelSliced) GetId() int          { return m.id }
