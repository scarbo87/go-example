package repository

import (
	"sync"
)

type (
	IRepositorySliced interface {
		GetCache() interface{}
		FindInCache(id int) []IModelSliced
		AddToCache(v IModelSliced)
		ClearCache()
		Len() int
	}

	RepositorySliced struct {
		cache      map[int][]IModelSliced
		cacheMutex *sync.RWMutex
		isInit     bool
		lenght     int
	}

	IModelSliced interface {
		GetRelationKey() int
		GetId() int
		IsActive() bool
	}
)

func (m *RepositorySliced) init() {
	if m.isInit != true {
		m.cacheMutex = &sync.RWMutex{}
		m.cache = make(map[int][]IModelSliced)
		m.isInit = true
		m.lenght = 0
	}
}

func (m *RepositorySliced) GetCache() interface{} {
	m.init()
	return m.cache
}

func (m *RepositorySliced) FindInCache(id int) []IModelSliced {
	m.init()
	m.cacheMutex.RLock()
	var result []IModelSliced
	var t []IModelSliced
	defer m.cacheMutex.RUnlock()

	if _, ok := m.cache[id]; ok {
		result = m.cache[id]
	}

	for _, val := range result {
		if val.IsActive() {
			t = append(t, val)
		}
	}

	return t
}

func (m *RepositorySliced) AddToCache(v IModelSliced) {
	m.init()
	var res []IModelSliced
	m.cacheMutex.Lock()
	defer m.cacheMutex.Unlock()

	current := m.cache[v.GetId()]
	m.lenght = m.lenght - len(current)
	for _, val := range current {
		if val.GetRelationKey() != v.GetRelationKey() {
			res = append(res, val)
		}
	}
	m.lenght = m.lenght + len(res) + 1
	m.cache[v.GetId()] = append(res, v)
}

func (m *RepositorySliced) ClearCache() {
	m.init()
	m.cacheMutex.Lock()
	defer m.cacheMutex.Unlock()

	m.cache = make(map[int][]IModelSliced)
}

func (m *RepositorySliced) Len() int {
	return m.lenght
}
