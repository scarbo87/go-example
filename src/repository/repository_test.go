package repository

import (
	"testing"
	"fmt"
)

func TestRepositoryInit(t *testing.T) {
	var v Repository
	v = Repository{}
	v.GetCache()
	if !v.isInit {
		t.Error("Base Model should be inited before GetCache")
	}
}

func TestRepositoryGet(t *testing.T) {
	var v Repository

	v = Repository{}
	res := v.FindInCache(1)
	if !v.isInit {
		t.Error("Base Model should be inited before FindInCache")
	}

	if res != nil {
		t.Error("Not empty result")
	}

	v = Repository{}
	v.AddToCache(testModel{true, 1, 2})
	if !v.isInit {
		t.Error("Base Model should be inited before GetCache")
	}
	res = v.FindInCache(1)
	if res == nil {
		t.Error("Empty result after put")
	}

}

func TestRepositoryGetInactive(t *testing.T) {
	var v Repository

	v = Repository{}
	v.AddToCache(testModel{false, 1, 2})

	res := v.FindInCache(1)
	if res != nil {
		t.Error("Found inactive result")
	}

	if v.Len() != 1 {
		t.Error("Error result count")
	}
}

func TestRepositoryClear(t *testing.T) {
	var v Repository

	v = Repository{}
	v.AddToCache(testModel{true, 1, 2})

	v.ClearCache()
	res := v.FindInCache(1)

	if res != nil {
		t.Error("Not empty result after clear")
	}
}

func TestAltIndex(t *testing.T) {
	var v Repository

	v = Repository{}
	v.RegisterIntIndex("test", AltIndexTest)
	v.RegisterStringIndex("testString", AltStringTest)

	v.AddToCache(testModel{true, 1, 2})

	res := v.FindIndex("test", 2, true)
	if res == nil {
		t.Error("Empty result after put")
	}

	v.AddToCache(testModel{true, 1, 3})

	res = v.FindIndex("test", 2, true)
	if res != nil {
		t.Error("Not empty result after put another alter index")
	}

	res = v.FindIndex("test", 3, true)
	if res == nil {
		t.Error("Empty result after one more")
	}
	res = v.FindIndex("testString", "3", true)
	if res == nil {
		t.Error("Empty result in string alter index")
	}
}

func TestPanic(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	v := Repository{}
	v.FindIndex("test", true, true)

}

func AltIndexTest(p IModel) int {
	return p.(testModel).altId
}

func AltStringTest(p IModel) string {
	return fmt.Sprintf("%d", p.(testModel).altId)
}

type testModel struct {
	active bool
	id     int
	altId  int
}

func (m testModel) IsActive() bool     { return m.active }
func (m testModel) GetId() int         { return m.id }
func (m testModel) SchemaName() string { return "Foo" }
func (m testModel) TableName() string  { return "Bar" }
