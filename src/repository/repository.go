package repository

import (
	"sync"
)

const StatusActive = "Active"

type (
	IRepository interface {
		RegisterIntIndex(string string, AltIndex AltIntIndex)
		RegisterStringIndex(string string, AltIndex AltStringIndex)

		GetCache() interface{}
		FindInCache(id int) IModel
		FindIndex(index string, id interface{}, onlyActive bool) IModel
		AddToCache(v IModel)
		ClearCache()
		Len() int
	}

	Repository struct {
		cache      map[int]*IModel
		cacheMutex *sync.RWMutex
		isInit     bool

		altIntIndex map[string]AltIntIndex
		altIntCache map[string]map[int]*IModel

		altStringIndex map[string]AltStringIndex
		altStringCache map[string]map[string]*IModel
	}

	IModel interface {
		IsActive() bool
		GetId() int
		SchemaName() string
		TableName() string
	}

	AltIntIndex func(orm IModel) int
	AltStringIndex func(orm IModel) string
)

func (r *Repository) init() {
	if r.isInit != true {
		r.cacheMutex = &sync.RWMutex{}
		r.cache = make(map[int]*IModel)
		r.isInit = true
		r.altIntIndex = make(map[string]AltIntIndex)
		r.altIntCache = make(map[string]map[int]*IModel)

		r.altStringIndex = make(map[string]AltStringIndex)
		r.altStringCache = make(map[string]map[string]*IModel)
	}
}

func FullName(model IModel) string {
	return model.SchemaName() + "." + model.TableName()
}

func (r *Repository) GetCache() interface{} {
	r.init()
	return r.cache
}

func (r *Repository) RegisterIntIndex(string string, AltIndex AltIntIndex) {
	r.init()
	r.altIntIndex[string] = AltIndex
}

func (r *Repository) RegisterStringIndex(string string, AltIndex AltStringIndex) {
	r.init()
	r.altStringIndex[string] = AltIndex
}

func (r *Repository) FindInCache(id int) IModel {
	r.init()
	r.cacheMutex.RLock()
	defer r.cacheMutex.RUnlock()

	if resultVal, ok := r.cache[id]; ok {
		result := (*resultVal).(IModel)
		if result.IsActive() {
			return result
		}

	}

	return nil

}

func (r *Repository) FindIndex(index string, id interface{}, onlyActive bool) IModel {
	r.init()
	r.cacheMutex.RLock()
	defer r.cacheMutex.RUnlock()

	switch id.(type) {
	case int:
		if _, ok := r.altIntIndex[index]; ok {
			if resultVal, ok := r.altIntCache[index][id.(int)]; ok {
				result := *resultVal
				if !onlyActive || result.IsActive() {
					return result
				}
			}
		}
	case string:
		if _, ok := r.altStringIndex[index]; ok {
			if resultVal, ok := r.altStringCache[index][id.(string)]; ok {
				result := *resultVal
				if !onlyActive || result.IsActive() {
					return result
				}
			}
		}
	default:
		panic("Only string or int key are available.")
	}

	return nil

}

func (r *Repository) AddToCache(v IModel) {
	r.init()
	r.cacheMutex.Lock()
	defer r.cacheMutex.Unlock()

	if oldVal, ok := r.cache[v.GetId()]; ok {
		for id, alterIndexFunc := range r.altIntIndex {
			intIdToDelete := alterIndexFunc(*oldVal)
			delete(r.altIntCache[id], intIdToDelete)
		}

		for id, alterIndexFunc := range r.altStringIndex {
			stringIdToDelete := alterIndexFunc(*oldVal)
			delete(r.altStringCache[id], stringIdToDelete)
		}
	}

	r.cache[v.GetId()] = &v

	for id, val := range r.altIntIndex {
		if _, ok := r.altIntCache[id]; !ok {
			r.altIntCache[id] = make(map[int]*IModel)
		}
		r.altIntCache[id][val(v)] = &v
	}

	for id, val := range r.altStringIndex {
		if _, ok := r.altStringCache[id]; !ok {
			r.altStringCache[id] = make(map[string]*IModel)
		}
		r.altStringCache[id][val(v)] = &v
	}

}

func (r *Repository) ClearCache() {
	r.init()
	r.cacheMutex.Lock()
	defer r.cacheMutex.Unlock()

	r.cache = make(map[int]*IModel)
	r.altIntCache = make(map[string]map[int]*IModel)
	r.altStringCache = make(map[string]map[string]*IModel)
}

func (r *Repository) Len() int {
	return len(r.cache)
}
