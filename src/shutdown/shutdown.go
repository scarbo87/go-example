package shutdown

import (
	"os"
	"os/signal"
	"syscall"
	"time"
	"github.com/siddontang/go-log/loggers"
)

func New(log loggers.Advanced) *Waiter {
	return &Waiter{log: log}
}

type Waiter struct {
	log             loggers.Advanced
	serviceRegistry []*Control
	isSwitchingOff  bool
}

func (m *Waiter) IsSwitchingOff() bool {
	return m.isSwitchingOff
}

func (m *Waiter) AddService(name string) (*Control) {
	inChannel := make(chan bool, 1)
	service := &Control{serviceName: name, inChannel: inChannel, waiter: m}

	m.serviceRegistry = append(m.serviceRegistry, service)
	return service
}

func (m *Waiter) GracefulShutdown() {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	go func() {
		s := <-signalChan
		m.log.Infof("Got exit signal %d pid %d", s, os.Getpid())
		m.isSwitchingOff = true
		for _, item := range m.serviceRegistry {
			item.inChannel <- true
		}

		m.waitingServices()
		m.log.Infof("Graceful shutdown pid %d", os.Getpid())
		os.Exit(0)

	}()
}

func (m *Waiter) waitingServices() {
	for {
		i := 0
		for _, item := range m.serviceRegistry {
			if !item.isWorkDone() {
				i++
			}
		}
		if i > 0 {
			m.log.Infof("Services are in progress (count %d)", i)
			time.Sleep(time.Second)
		} else {
			return
		}
	}
}

type Control struct {
	serviceName string
	inChannel   chan bool
	workDone    bool
	waiter      *Waiter
}

func (m *Control) isWorkDone() bool {
	return m.workDone
}

func (m *Control) Done() {
	m.workDone = true
}

func (m *Control) IsSwitchingOff() bool {
	return m.waiter.isSwitchingOff
}

func (m *Control) GetChannel() chan bool {
	return m.inChannel
}
