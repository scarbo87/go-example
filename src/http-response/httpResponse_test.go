package http_response

import (
	"testing"
	"net/http"
	"net/http/httptest"
)

func TestErrorStates(t *testing.T) {
	json := JSON()

	json.ServerError()
	if json.body.(string) != http.StatusText(http.StatusInternalServerError) && json.status != http.StatusInternalServerError {
		t.Error("ServerErorState jsonMessage error")
	}

	json.BadRequestError()
	if json.body.(string) != http.StatusText(http.StatusBadRequest) && json.status != http.StatusBadRequest {
		t.Error("BadRequestError jsonMessage error")
	}

	json.ForbiddenError()
	if json.body.(string) != http.StatusText(http.StatusForbidden) && json.status != http.StatusForbidden {
		t.Error("ForbiddenError jsonMessage error")
	}

	json.TooManyRequestsError()
	if json.body.(string) != http.StatusText(http.StatusTooManyRequests) && json.status != http.StatusTooManyRequests {
		t.Error("TooManyRequestsError jsonMessage error")
	}

	json.NotFoundError()
	if json.body.(string) != http.StatusText(http.StatusNotFound) && json.status != http.StatusNotFound {
		t.Error("NotFoundError jsonMessage error")
	}
}

func TestCustomErrorState(t *testing.T) {
	json := JSON()
	message := "Test"
	status := 100

	json.CustomResult(message, status)
	if json.body.(string) != message && json.status != status {
		t.Error("ServerErorState jsonMessage error")
	}
}

func TestJSONRawResponse_Render(t *testing.T) {
	w := httptest.ResponseRecorder{}
	JSON().NotFoundError().Render(&w)
	if w.Code != http.StatusNotFound {
		t.Error("JSON render problem")
	}

	defer func() {
		r := recover()
		if r == nil {
			t.Errorf("Trash value to json worked. json")
		}
	}()
	value := make(chan int)
	JSON().CustomResult(value, 100).Render(&w)
}

func TestSuccessResponse(t *testing.T) {
	json := JSON()
	message := "1"

	json.SuccessResult(message)
	if json.body.(string) != message && json.status != http.StatusOK {
		t.Error("SuccessResult jsonMessage error")
	}
}
