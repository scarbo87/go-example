package routing

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

// Slice of default middleware
var defaultMiddleware []MiddlewareFunc

// Template for middleware functions
type MiddlewareFunc func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc)

// Create slice of middleware functions from functions + add default middleware
func MiddlewareChain(functions ...MiddlewareFunc) []MiddlewareFunc {
	return append(getDefaultMiddleware(), functions...)
}

// Set slice of default middleware
func SetDefaultMiddleware(middleware []MiddlewareFunc) {
	defaultMiddleware = middleware
}

// Return slice of default middleware
func getDefaultMiddleware() []MiddlewareFunc {
	return defaultMiddleware
}

// Extract params from request
func getUrlParams(router *httprouter.Router, req *http.Request) httprouter.Params {
	_, params, _ := router.Lookup(req.Method, req.URL.Path)
	return params
}

// Add params from request to the handler
func callWithParams(router *httprouter.Router, handler func(w http.ResponseWriter, r *http.Request, ps httprouter.Params)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		params := getUrlParams(router, r)
		handler(w, r, params)
	}
}
