package routing

import (
	"net/http"
	"testing"
)

func TestSetDefaultMiddleware(t *testing.T) {
	startLength := len(defaultMiddleware)
	newDefaultMiddleware := []MiddlewareFunc{func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		next(w, r)
	}}
	SetDefaultMiddleware(newDefaultMiddleware)
	endLength := len(defaultMiddleware)
	if endLength-startLength != 1 {
		t.Fatal("SetDefaultMiddleware didn't set")
	}
}

func TestMiddlewareChain(t *testing.T) {
	newDefaultMiddleware := []MiddlewareFunc{func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		next(w, r)
	}}
	SetDefaultMiddleware(newDefaultMiddleware)

	newMiddleware := func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		next(w, r)
	}
	chain := MiddlewareChain(newMiddleware)
	if len(chain) != 2 {
		t.Fatal("Item wasn't added to the chain")
	}
}
