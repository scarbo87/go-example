package routing

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"testing"
)

type mockResponseWriter struct{}

func (m *mockResponseWriter) Header() (h http.Header) {
	return http.Header{}
}

func (m *mockResponseWriter) Write(p []byte) (n int, err error) {
	return len(p), nil
}

func (m *mockResponseWriter) WriteString(s string) (n int, err error) {
	return len(s), nil
}

func (m *mockResponseWriter) WriteHeader(int) {}

func TestNewRouter(t *testing.T) {
	// Show middleware was added
	middlewareCount := 0
	middlewareChain := []MiddlewareFunc{func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		middlewareCount++
		next(w, r)
	}}

	// Show route was serve
	routedCounter := 0
	// Check params send
	paramsWasAdded := false
	var routes = Routes{
		Route{
			"Test1",
			"GET",
			"/testOne",
			func(res http.ResponseWriter, req *http.Request, _ httprouter.Params) {
				routedCounter++
			},
			middlewareChain,
		},
		Route{
			"Test2",
			"POST",
			"/testTwo/:test",
			func(res http.ResponseWriter, req *http.Request, param httprouter.Params) {
				routedCounter++
				if "" != param.ByName("test") {
					paramsWasAdded = true
				}
			},
			middlewareChain,
		},
	}

	w := new(mockResponseWriter)

	// Send request to the first route
	router := NewRouter(routes)
	req, _ := http.NewRequest("GET", "/testOne", nil)
	router.ServeHTTP(w, req)

	// Send request to the second route
	reqTwo, _ := http.NewRequest("POST", "/testTwo/2", nil)
	router.ServeHTTP(w, reqTwo)

	// Check non-exist route. Counters must not change.
	reqNotFount, _ := http.NewRequest("GET", "/test404", nil)
	router.ServeHTTP(w, reqNotFount)

	if middlewareCount != len(routes) {
		t.Fatal("Middleware hadn't been attached")
	}

	if routedCounter != len(routes) {
		t.Fatal("Requests didn't find routes")
	}

	if paramsWasAdded != true {
		t.Fatal("Params weren't send")
	}

}

func TestDefaultMiddlewareExecution(t *testing.T) {
	middlewareCount := 0
	newDefaultMiddleware := []MiddlewareFunc{func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		middlewareCount++
		next(w, r)
	}}
	SetDefaultMiddleware(newDefaultMiddleware)

	var routes = Routes{
		Route{
			"Test1",
			"GET",
			"/testOne",
			func(res http.ResponseWriter, req *http.Request, _ httprouter.Params) {

			},
			MiddlewareChain(),
		},
		Route{
			"Test2",
			"POST",
			"/testTwo/:test",
			func(res http.ResponseWriter, req *http.Request, param httprouter.Params) {

			},
			MiddlewareChain(),
		},
	}

	w := new(mockResponseWriter)

	// Send request to the first route
	router := NewRouter(routes)
	req, _ := http.NewRequest("GET", "/testOne", nil)
	router.ServeHTTP(w, req)

	// Send request to the second route
	reqTwo, _ := http.NewRequest("POST", "/testTwo/2", nil)
	router.ServeHTTP(w, reqTwo)

	// Check non-exist route. Counter must not change.
	reqNotFount, _ := http.NewRequest("GET", "/test404", nil)
	router.ServeHTTP(w, reqNotFount)

	if middlewareCount != len(routes) {
		t.Fatal("Default middleware hadn't been called")
	}
}
