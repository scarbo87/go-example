package routing

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/urfave/negroni"
	"net/http"
)

// Route main struct
type Route struct {
	Name        string            // Route name
	Method      string            // Route allowed methods (GET, POST, etc)
	Pattern     string            // Route URL pattern
	HandlerFunc httprouter.Handle // Route action
	Middleware  []MiddlewareFunc  // Route middleware slice
}

// Slice of Routes
type Routes []Route

// Create and return new Router
func NewRouter(routes Routes) *httprouter.Router {
	router := httprouter.New()
	router.NotFound = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, fmt.Sprintf("%s %d", http.StatusText(http.StatusNotFound), http.StatusNotFound), http.StatusNotFound)
	})

	for _, route := range routes {
		handler := negroni.New()
		for _, middleware := range route.Middleware {
			handler.Use(negroni.HandlerFunc(middleware))
		}
		handler.UseHandlerFunc(callWithParams(router, route.HandlerFunc))
		router.Handler(route.Method,
			route.Pattern,
			handler)
	}
	return router
}
