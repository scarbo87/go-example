package binlog

import (
	"github.com/siddontang/go-mysql/canal"
	"github.com/siddontang/go-mysql/schema"
	"testing"
	"time"
	"github.com/stretchr/testify/assert"
)

type (
	testStruct struct {
		Int             int       `gorm:"column:int"`
		Bool            bool      `gorm:"column:bool"`
		Float           float64   `gorm:"column:float"`
		Enum            string    `gorm:"column:enum"`
		String          string    `gorm:"column:string"`
		Time            time.Time `gorm:"column:time"`
		EnumNull        string    `gorm:"column:enum_null"`
		ByteText        string    `gorm:"column:byte_text"`
		WillNotParse    int
		WillNotParseAlt int       `gorm:"column"`
	}

	numberStruct struct {
		Int     int  `gorm:"column:int"`
		Int8    int  `gorm:"column:int_8"`
		Int16   int  `gorm:"column:int_16"`
		Int32   int  `gorm:"column:int_32"`
		Int64   int  `gorm:"column:int_64"`
		IntNil  int  `gorm:"column:int_nil"`
		Uint    uint `gorm:"column:uint"`
		Uint8   uint `gorm:"column:uint_8"`
		Uint16  uint `gorm:"column:uint_16"`
		Uint32  uint `gorm:"column:uint_32"`
		Uint64  uint `gorm:"column:uint_64"`
		UintNil uint `gorm:"column:uint_nil"`
	}

	withJsonStruct struct {
		Int        int               `gorm:"column:int"`
		StructData JsonStruct        `gorm:"column:struct_data;fromJson"`
		MapData    map[string]string `gorm:"column:map_data;fromJson"`
		SliceData  []int             `gorm:"column:slice_data;fromJson"`
	}

	JsonStruct struct {
		Test string `json:"test"`
		Int  int    `json:"int"`
	}
)

func TestGetData_Insert(t *testing.T) {

	model := testStruct{}

	insertRows := []interface{}{
		1,
		1,
		1.123,
		int64(1),
		"test text",
		"2018-02-16 14:28:09",
		nil,
		[]byte("test text"),
	}
	rows := [][]interface{}{insertRows}

	columns := []schema.TableColumn{
		{Name: "int", Type: schema.TYPE_NUMBER},
		{Name: "bool", Type: schema.TYPE_NUMBER},
		{Name: "float", Type: schema.TYPE_FLOAT},
		{Name: "enum", Type: schema.TYPE_ENUM, EnumValues: []string{"Active", "Deleted"},},
		{Name: "string", Type: schema.TYPE_STRING},
		{Name: "time", Type: schema.TYPE_TIMESTAMP},
		{Name: "enum_null", Type: schema.TYPE_ENUM, EnumValues: []string{"Active", "Deleted"},},
		{Name: "byte_text", Type: schema.TYPE_STRING},
	}

	table := schema.Table{Schema: "test", Name: "test", Columns: columns}
	e := canal.RowsEvent{Table: &table, Action: canal.InsertAction, Rows: rows}
	err := FillModel(&model, &e, 0)
	assert.NoError(t, err)

	timeValue, _ := time.Parse("2006-01-02 15:04:05", insertRows[5].(string))
	assert.Equal(t, insertRows[0], model.Int)
	assert.Equal(t, true, model.Bool)
	assert.Equal(t, insertRows[2], model.Float)
	assert.Equal(t, "Active", model.Enum)
	assert.Equal(t, insertRows[4], model.String)
	assert.Equal(t, "", model.EnumNull)
	assert.Equal(t, timeValue.Unix(), model.Time.Unix())
}

func TestGetData_Update(t *testing.T) {

	model := testStruct{}

	insertRows := []interface{}{
		1,
		0,
		1.123,
		int64(1),
		"test text",
		"2018-02-16 14:28:09",
		int64(1),
		[]byte("test text"),
	}
	updateRows := []interface{}{
		3,
		1,
		2.234,
		int64(2),
		"test2 text2",
		"2018-02-16 15:28:09",
		nil,
		[]byte("test2 text2"),
	}
	rows := [][]interface{}{insertRows, updateRows}

	columns := []schema.TableColumn{
		{Name: "int", Type: schema.TYPE_NUMBER},
		{Name: "bool", Type: schema.TYPE_NUMBER},
		{Name: "float", Type: schema.TYPE_FLOAT},
		{Name: "enum", Type: schema.TYPE_ENUM, EnumValues: []string{"Active", "Deleted"},},
		{Name: "string", Type: schema.TYPE_STRING},
		{Name: "time", Type: schema.TYPE_TIMESTAMP},
		{Name: "enum_null", Type: schema.TYPE_ENUM, EnumValues: []string{"Active", "Deleted"},},
		{Name: "byte_text", Type: schema.TYPE_STRING},
	}

	table := schema.Table{Schema: "test", Name: "test", Columns: columns}
	e := canal.RowsEvent{Table: &table, Action: canal.UpdateAction, Rows: rows}
	err := FillModel(&model, &e, 1)
	assert.NoError(t, err)

	timeValue, _ := time.Parse("2006-01-02 15:04:05", updateRows[5].(string))
	assert.Equal(t, updateRows[0], model.Int)
	assert.Equal(t, true, model.Bool)
	assert.Equal(t, updateRows[2], model.Float)
	assert.Equal(t, "Deleted", model.Enum)
	assert.Equal(t, updateRows[4], model.String)
	assert.Equal(t, "", model.EnumNull)
	assert.Equal(t, timeValue.Unix(), model.Time.Unix())
}

func TestGetData_Number(t *testing.T) {

	model := numberStruct{}

	insertRows := []interface{}{
		int(-1),
		int8(-10),
		int16(-100),
		int32(-1000),
		int64(-10000),
		nil,
		uint(1),
		uint8(10),
		uint16(100),
		uint32(1000),
		uint64(10000),
		nil,
	}
	rows := [][]interface{}{insertRows}

	columns := []schema.TableColumn{
		{Name: "int", Type: schema.TYPE_NUMBER},
		{Name: "int_8", Type: schema.TYPE_NUMBER},
		{Name: "int_16", Type: schema.TYPE_NUMBER},
		{Name: "int_32", Type: schema.TYPE_NUMBER},
		{Name: "int_64", Type: schema.TYPE_NUMBER},
		{Name: "int_nil", Type: schema.TYPE_NUMBER},
		{Name: "uint", Type: schema.TYPE_NUMBER},
		{Name: "uint_8", Type: schema.TYPE_NUMBER},
		{Name: "uint_16", Type: schema.TYPE_NUMBER},
		{Name: "uint_32", Type: schema.TYPE_NUMBER},
		{Name: "uint_64", Type: schema.TYPE_NUMBER},
		{Name: "uint_nil", Type: schema.TYPE_NUMBER},
	}

	table := schema.Table{Schema: "test", Name: "test", Columns: columns}
	e := canal.RowsEvent{Table: &table, Action: canal.InsertAction, Rows: rows}
	err := FillModel(&model, &e, 0)
	assert.NoError(t, err)

	assert.Equal(t, insertRows[0].(int), model.Int)
	assert.Equal(t, int(insertRows[1].(int8)), model.Int8)
	assert.Equal(t, int(insertRows[2].(int16)), model.Int16)
	assert.Equal(t, int(insertRows[3].(int32)), model.Int32)
	assert.Equal(t, int(insertRows[4].(int64)), model.Int64)
	assert.Equal(t, int(0), model.IntNil)
	assert.Equal(t, insertRows[6].(uint), model.Uint)
	assert.Equal(t, uint(insertRows[7].(uint8)), model.Uint8)
	assert.Equal(t, uint(insertRows[8].(uint16)), model.Uint16)
	assert.Equal(t, uint(insertRows[9].(uint32)), model.Uint32)
	assert.Equal(t, uint(insertRows[10].(uint64)), model.Uint64)
	assert.Equal(t, uint(0), model.UintNil)
}

func TestGetData_Json(t *testing.T) {

	model := withJsonStruct{}

	insertRows := []interface{}{
		1,
		`{"int":1,"test":"test"}`,
		`{"a":"a","b":"b"}`,
		`[2,4,6}`,
	}
	rows := [][]interface{}{insertRows}

	columns := []schema.TableColumn{
		{Name: "int", Type: schema.TYPE_NUMBER},
		{Name: "struct_data", Type: schema.TYPE_STRING},
		{Name: "map_data", Type: schema.TYPE_STRING},
		{Name: "slice_data", Type: schema.TYPE_STRING},
	}

	table := schema.Table{Schema: "test", Name: "test", Columns: columns}
	e := canal.RowsEvent{Table: &table, Action: canal.InsertAction, Rows: rows}
	err := FillModel(&model, &e, 0)
	assert.NoError(t, err)

	if model.StructData.Test != "test" || model.StructData.Int != 1 {
		t.Fatal("Struct from json parsing failed.")
	}
	if len(model.SliceData) != 3 || model.SliceData[0] != 2 || model.SliceData[2] != 6 {
		t.Fatal("Sliced json parsing failed.")
	}
	if val, ok := model.MapData["a"]; ok && val != "a" && len(model.MapData) != 2 {
		t.Fatal("Map json parsing failed.")
	}
}

func TestGetData_ErrorNoColumn(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()

	type invalidStruct struct {
		Int int `gorm:"column:id"`
	}
	model := invalidStruct{}

	insertRows := []interface{}{1}
	rows := [][]interface{}{insertRows}

	columns := []schema.TableColumn{
		{Name: "int", Type: schema.TYPE_NUMBER},
	}

	table := schema.Table{Schema: "test", Name: "test", Columns: columns}
	e := canal.RowsEvent{Table: &table, Action: canal.InsertAction, Rows: rows}

	FillModel(&model, &e, 0)
}

func TestGetData_ErrorNotSupportedType(t *testing.T) {
	type invalidStruct struct {
		Int complex64 `gorm:"column:id"`
	}
	model := invalidStruct{}

	insertRows := []interface{}{1}
	rows := [][]interface{}{insertRows}

	columns := []schema.TableColumn{
		{Name: "id", Type: schema.TYPE_NUMBER},
	}

	table := schema.Table{Schema: "test", Name: "test", Columns: columns}
	e := canal.RowsEvent{Table: &table, Action: canal.InsertAction, Rows: rows}

	err := FillModel(&model, &e, 0)
	assert.EqualError(t, err, "This type complex64 is not supported")
}
