package binlog

import (
	"github.com/json-iterator/go"
	"github.com/siddontang/go-mysql/canal"
	"github.com/siddontang/go-mysql/schema"
	"reflect"
	"strings"
	"time"
	"github.com/juju/errors"
	"fmt"
)

const (
	ErrIntType   = "Not int type"
	ErrUintType  = "Not uint type"
	ErrFloatType = "Not float type"
	ErrTimeType  = "Not dateTime type"
)

var (
	TagName = "gorm"
)

func FillModel(model interface{}, e *canal.RowsEvent, n int) error {

	var columnName string
	var ok bool
	v := reflect.ValueOf(model)
	s := reflect.Indirect(v)
	t := s.Type()
	num := t.NumField()

	for i := 0; i < num; i++ {

		parsedTag := parseTagSetting(t.Field(i).Tag)
		name := s.Field(i).Type().Name()

		if columnName, ok = parsedTag["COLUMN"]; !ok || columnName == "COLUMN" {
			continue
		}

		switch name {
		case "bool":
			v := convertToBool(e, n, columnName)
			s.Field(i).SetBool(v)
		case "int":
			v := convertToInt64(e, n, columnName)
			s.Field(i).SetInt(v)
		case "uint":
			v := convertToUint64(e, n, columnName)
			s.Field(i).SetUint(v)
		case "string":
			v := convertToString(e, n, columnName)
			s.Field(i).SetString(v)
		case "Time":
			v, err := convertToTime(e, n, columnName)
			if err != nil {
				return err
			}
			s.Field(i).Set(reflect.ValueOf(v))
		case "float64":
			v := convertToFloat(e, n, columnName)
			s.Field(i).SetFloat(v)
		default:
			if _, ok := parsedTag["FROMJSON"]; ok {

				newObject := reflect.New(s.Field(i).Type()).Interface()
				json := convertToString(e, n, columnName)
				jsoniter.Unmarshal([]byte(json), &newObject)
				s.Field(i).Set(reflect.ValueOf(newObject).Elem().Convert(s.Field(i).Type()))
			} else {
				return errors.Errorf("This type %s is not supported", name)
			}
		}
	}

	return nil
}

func convertToInt64(e *canal.RowsEvent, n int, columnName string) int64 {

	columnId := getIdByName(e, columnName)
	if e.Table.Columns[columnId].Type != schema.TYPE_NUMBER {
		panic(ErrIntType)
	}

	switch e.Rows[n][columnId].(type) {
	case int8:
		return int64(e.Rows[n][columnId].(int8))
	case int16:
		return int64(e.Rows[n][columnId].(int16))
	case int32:
		return int64(e.Rows[n][columnId].(int32))
	case int64:
		return e.Rows[n][columnId].(int64)
	case int:
		return int64(e.Rows[n][columnId].(int))
	}

	// если поле null - возвращаем 0
	return 0
}

func convertToUint64(e *canal.RowsEvent, n int, columnName string) uint64 {

	columnId := getIdByName(e, columnName)
	if e.Table.Columns[columnId].Type != schema.TYPE_NUMBER {
		panic(ErrUintType)
	}

	switch e.Rows[n][columnId].(type) {
	case uint8:
		return uint64(e.Rows[n][columnId].(uint8))
	case uint16:
		return uint64(e.Rows[n][columnId].(uint16))
	case uint32:
		return uint64(e.Rows[n][columnId].(uint32))
	case uint64:
		return uint64(e.Rows[n][columnId].(uint64))
	case uint:
		return uint64(e.Rows[n][columnId].(uint))
	}

	// если поле null - возвращаем 0
	return 0
}

func convertToFloat(e *canal.RowsEvent, n int, columnName string) float64 {

	columnId := getIdByName(e, columnName)
	if e.Table.Columns[columnId].Type != schema.TYPE_FLOAT {
		panic(ErrFloatType)
	}

	switch e.Rows[n][columnId].(type) {
	case float32:
		return float64(e.Rows[n][columnId].(float32))
	case float64:
		return float64(e.Rows[n][columnId].(float64))
	}

	// если поле null - возвращаем 0.0
	return float64(0)
}

func convertToString(e *canal.RowsEvent, n int, columnName string) string {

	columnId := getIdByName(e, columnName)
	if e.Table.Columns[columnId].Type == schema.TYPE_ENUM {
		values := e.Table.Columns[columnId].EnumValues
		if len(values) == 0 {
			return ""
		}
		if e.Rows[n][columnId] == nil {
			//Если Enum is null ставим пустую строку
			return ""
		}

		return values[e.Rows[n][columnId].(int64)-1]
	}

	value := e.Rows[n][columnId]
	switch value := value.(type) {
	case []byte:
		return string(value)
	case string:
		return value
	}

	// если поле null - возвращаем ""
	return ""
}

func convertToBool(e *canal.RowsEvent, n int, columnName string) bool {

	val := convertToInt64(e, n, columnName)
	if val == 1 {
		return true
	}

	return false
}

func convertToTime(e *canal.RowsEvent, n int, columnName string) (time.Time, error) {

	columnId := getIdByName(e, columnName)
	if e.Table.Columns[columnId].Type != schema.TYPE_TIMESTAMP {
		panic(ErrTimeType)
	}

	return time.Parse("2006-01-02 15:04:05", e.Rows[n][columnId].(string))
}

func getIdByName(e *canal.RowsEvent, name string) int {

	for id, value := range e.Table.Columns {
		if value.Name == name {
			return id
		}
	}

	panic(fmt.Sprintf("There is no column %s in table %s.%s", name, e.Table.Schema, e.Table.Name))
}

func parseTagSetting(tags reflect.StructTag) map[string]string {

	setting := map[string]string{}
	for _, str := range []string{tags.Get("sql"), tags.Get(TagName)} {
		tags := strings.Split(str, ";")
		for _, value := range tags {
			v := strings.Split(value, ":")
			k := strings.TrimSpace(strings.ToUpper(v[0]))
			if len(v) >= 2 {
				setting[k] = strings.Join(v[1:], ":")
			} else {
				setting[k] = k
			}
		}
	}

	return setting
}
