package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"time"
)

type Config struct {
	Env   string `yaml:"env"`
	Debug bool   `yaml:"debug"`
	Server struct {
		HttpAddress   string `yaml:"http_server_address"`
		ExpvarAddress string `yaml:"expvar_server_address"`
	} `yaml:"server"`
	MysqlSlave struct {
		Host     string `yaml:"host"`
		Port     uint16 `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		Charset  string `yaml:"charset"`
	} `yaml:"mysql_slave"`
	MysqlBinLog struct {
		Flavor          string        `yaml:"flavor"`
		HeartbeatTable  string        `yaml:"heartbeat_table"`
		HeartbeatPeriod time.Duration `yaml:"heartbeat_period"`
		ReadTimeout     time.Duration `yaml:"read_timeout"`
		Databases       []string      `yaml:"databases"`
		IgnoreTables    []string      `yaml:"ignore_tables"`
	} `yaml:"mysql_binlog"`
}

func (m *Config) Parse(configPath string) error {
	configYml, err := ioutil.ReadFile(configPath)
	if err != nil {
		return err
	}

	return yaml.Unmarshal(configYml, m)
}

func (m *Config) IsDev() bool {
	return m.Env == "dev"
}

func (m *Config) GetHTTPAddress() string {
	return m.Server.HttpAddress
}

func (m *Config) GetExpvarAddress() string {
	return m.Server.ExpvarAddress
}
