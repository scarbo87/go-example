package model

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
	rep "go-example/src/repository"
	"github.com/jinzhu/gorm"
	"go-example/src/config"
	"github.com/siddontang/go-log/loggers"
	"fmt"
)

var (
	RoomRep = &RoomRepository{}
	UserRep = NewUserRepository(RoomRep)
)

var RepPull = map[string]DbRep{
	rep.FullName(RoomRep.GetObject()): RoomRep,
	rep.FullName(UserRep.GetObject()): UserRep,
}

type DbRep interface {
	InitialSelect(db *gorm.DB)
	GetObject() rep.IModel
	Len() int
	GetCache() interface{}
	AddToCache(v rep.IModel)
}

func InitialSelect(cfg config.Config, log loggers.Advanced) {
	for _, val := range RepPull {
		dbSelect(val, cfg, log)
	}
}

func dbSelect(r DbRep, cfg config.Config, log loggers.Advanced) {
	db, err := getMysqlReadConnection(r.GetObject().SchemaName(), cfg)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	db.SetLogger(log)
	db = db.LogMode(cfg.Debug)

	r.InitialSelect(db)
}

func getMysqlReadConnection(schemaName string, cfg config.Config) (*gorm.DB, error) {
	return gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s",
		cfg.MysqlSlave.User,
		cfg.MysqlSlave.Password,
		cfg.MysqlSlave.Host,
		cfg.MysqlSlave.Port,
		schemaName,
		cfg.MysqlSlave.Charset))
}
