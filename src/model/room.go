package model

import (
	rep "go-example/src/repository"
	"github.com/jinzhu/gorm"
)

type Room struct {
	Id     uint   `gorm:"column:id"`
	Name   string `gorm:"column:name"`
	Status string `gorm:"column:status"`
}

func (Room) SchemaName() string {
	return "Example"
}
func (Room) TableName() string {
	return "Room"
}
func (m Room) IsActive() bool {
	return m.Status == rep.StatusActive
}

func (m Room) GetId() int {
	return int(m.Id)
}

type RoomRepository struct {
	rep.Repository
}

func (m *RoomRepository) InitialSelect(db *gorm.DB) {
	var r []Room
	db.Find(&r, "status = ?", rep.StatusActive)

	for _, item := range r {
		m.AddToCache(item)
	}
}

func (m *RoomRepository) GetObject() rep.IModel {
	return Room{}
}
