package model

import (
	rep "go-example/src/repository"
	"github.com/jinzhu/gorm"
)

type User struct {
	Id        uint   `gorm:"column:id"`
	FirstName string `gorm:"column:first_name"`
	LastName  string `gorm:"column:last_name"`
	Email     string `gorm:"column:email"`
	RoomId    uint   `gorm:"column:room_id"`
	Status    string `gorm:"column:status"`
}

func (User) SchemaName() string {
	return "Example"
}
func (User) TableName() string {
	return "User"
}
func (m User) IsActive() bool {
	return m.Status == rep.StatusActive
}

func (m User) GetId() int {
	return int(m.Id)
}

type UserRepository struct {
	rep.Repository
	RoomRep *RoomRepository
}

func NewUserRepository(r *RoomRepository) *UserRepository {
	m := &UserRepository{}
	m.RoomRep = r
	m.RegisterIntIndex("RoomId", getRoomFromId)

	return m
}

func getRoomFromId(m rep.IModel) int {
	return int(m.(User).RoomId)
}

func (m *UserRepository) InitialSelect(db *gorm.DB) {
	var r []User
	db.Find(&r, "status = ?", rep.StatusActive)

	for _, item := range r {
		m.AddToCache(item)
	}
}

func (m *UserRepository) GetObject() rep.IModel {
	return User{}
}
