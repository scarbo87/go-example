package model

import (
	"fmt"
	"strconv"
	"runtime/debug"
	"time"

	"github.com/siddontang/go-mysql/canal"
	"github.com/siddontang/go-log/loggers"

	"go-example/src/metrics"
	"go-example/src/config"
	"go-example/src/binlog"
	rep "go-example/src/repository"
)

type BinLogHandler struct {
	canal.DummyEventHandler
	canal *canal.Canal
	cfg   config.Config
	log   loggers.Advanced
}

func NewHandler(cfg config.Config, log loggers.Advanced) *BinLogHandler {
	h := &BinLogHandler{log: log, cfg: cfg}
	return h
}

func (h *BinLogHandler) String() string {
	return "BinLogHandler"
}

func (h *BinLogHandler) Handle() {
	c, err := h.getDefaultCanal()
	if err != nil {
		// TODO надо юзать каналы
		h.log.Fatal(err)
	}

	coords, err := c.GetMasterPos()
	if err != nil {
		h.log.Fatal(err)
	}

	// TODO надо юзать каналы
	metrics.Canal = c

	c.SetEventHandler(h)
	c.RunFrom(coords)
}

func (h *BinLogHandler) getDefaultCanal() (*canal.Canal, error) {
	cfg := canal.NewDefaultConfig()
	cfg.Addr = fmt.Sprintf("%s:%s",
		h.cfg.MysqlSlave.Host,
		strconv.Itoa(int(h.cfg.MysqlSlave.Port)),
	)
	cfg.User = h.cfg.MysqlSlave.User
	cfg.Password = h.cfg.MysqlSlave.Password
	cfg.Flavor = h.cfg.MysqlBinLog.Flavor

	cfg.Dump.Databases = h.cfg.MysqlBinLog.Databases
	cfg.Dump.IgnoreTables = h.cfg.MysqlBinLog.IgnoreTables
	cfg.HeartbeatPeriod = h.cfg.MysqlBinLog.HeartbeatPeriod
	cfg.ReadTimeout = h.cfg.MysqlBinLog.ReadTimeout
	cfg.Dump.ExecutionPath = ""

	return canal.NewCanal(cfg)
}

func (h *BinLogHandler) OnRow(e *canal.RowsEvent) error {
	defer func() {
		if r := recover(); r != nil {
			h.log.Error(r, " ", string(debug.Stack()))
		}
	}()

	h.heartbeatMetrics(e)

	var n int
	var k int
	switch e.Action {
	case canal.DeleteAction:
		return nil
	case canal.UpdateAction:
		n = 1
		k = 2
	case canal.InsertAction:
		n = 0
		k = 1
	}

	for i := n; i < len(e.Rows); i += k {
		switch h.fullName(e) {
		case rep.FullName(User{}):
			data := User{}
			err := binlog.FillModel(&data, e, i)
			if err != nil {
				return err
			}
			UserRep.AddToCache(data)
		case rep.FullName(Room{}):
			data := Room{}
			err := binlog.FillModel(&data, e, i)
			if err != nil {
				return err
			}
			RoomRep.AddToCache(data)
		}
	}

	return nil
}

func (h *BinLogHandler) heartbeatMetrics(e *canal.RowsEvent) {
	if h.cfg.MysqlBinLog.HeartbeatTable == h.fullName(e) {
		if len(e.Rows) > 1 {
			t, err := time.ParseInLocation("2006-01-02 15:04:05", e.Rows[1][1].(string), time.Local)
			if err == nil {
				metrics.Heartbeat.Set(t.Unix())
			}
		}
	}
}

func (h *BinLogHandler) fullName(e *canal.RowsEvent) string {
	return e.Table.Schema + "." + e.Table.Name
}
