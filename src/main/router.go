package main

import (
	r "go-example/src/routing"
	h "go-example/src/handler"
)

var DefaultMiddlewares = []r.MiddlewareFunc{RecoverMiddleware, RequestStatMiddleware, SwitchingOffMiddleware}

func Routes() r.Routes {
	return r.Routes{
		r.Route{
			Name:        "getUser",
			Method:      "GET",
			Pattern:     "/users/:id",
			HandlerFunc: h.GetUser,
			Middleware:  r.MiddlewareChain(),
		},
		r.Route{
			Name:        "getRoom",
			Method:      "GET",
			Pattern:     "/rooms/:id",
			HandlerFunc: h.GetRoom,
			Middleware:  r.MiddlewareChain(),
		},
		r.Route{
			Name:        "look",
			Method:      "GET",
			Pattern:     "/cache",
			HandlerFunc: h.GetCache,
			Middleware:  r.MiddlewareChain(AdminMiddleware),
		},
	}
}
