package main

import (
	"net/http"
	"flag"

	"go-example/src/routing"
	"go-example/src/config"
	"go-example/src/metrics"
	"go-example/src/shutdown"

	"github.com/sirupsen/logrus"
	"go-example/src/model"
)

// эти переменные запишутся при компиляции с флагом -ldflags
var (
	version = "unknown"
)

var (
	login string
	pass  string

	configPath string
	log        *logrus.Logger
	waiter     *shutdown.Waiter
	cfg        config.Config
)

func init() {
	configPath = *flag.String("config", "./config/config.yml", "path to config file")
	login = *flag.String("login", "admin", "login for admin routes")
	pass = *flag.String("pass", "12345", "pass for admin routes")

	metrics.Version.Set(version)
	log = logrus.New()
	waiter = shutdown.New(log)
}

func main() {
	flag.Parse()
	err := cfg.Parse(configPath)
	if err != nil {
		log.Fatal(err)
	}

	go waiter.GracefulShutdown()

	logSettings()

	log.Info("Initial select starting")
	model.InitialSelect(cfg, log)

	log.Info("Binlog listen starting")
	go model.NewHandler(cfg, log).Handle()

	log.Infof("Expvar starting on %s", cfg.GetExpvarAddress())
	go http.ListenAndServe(cfg.GetExpvarAddress(), nil)

	log.Infof("Server starting on %s", cfg.GetHTTPAddress())
	routing.SetDefaultMiddleware(DefaultMiddlewares)
	log.Fatal(http.ListenAndServe(cfg.GetHTTPAddress(), routing.NewRouter(Routes())))
}

func logSettings() {
	if cfg.Debug {
		log.SetLevel(logrus.DebugLevel)
	} else {
		log.SetLevel(logrus.InfoLevel)
	}

	if cfg.IsDev() {
		logrus.SetFormatter(&logrus.TextFormatter{})
	} else {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	log.AddHook(&metrics.ExpvarHook{})
}
