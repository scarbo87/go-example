package main

import (
	"net/http"
	response "go-example/src/http-response"
	"runtime/debug"
	"time"
	"math/rand"
)

// Saves our application from a sudden death
func RecoverMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	defer func(w http.ResponseWriter, request *http.Request) {
		if r := recover(); r != nil {
			log.Error(r, " ", string(debug.Stack()))
			response.JSON().ServerError().Render(w)
		}
	}(w, r)

	next(w, r)
}

// Saves our application from a sudden death
func SwitchingOffMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if waiter.IsSwitchingOff() {
		response.JSON().ServerError().Render(w)
		return
	}
	next(w, r)
}

func AdminMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if cfg.IsDev() {
		next(w, r)
		return
	}

	// это пример! не для прода
	user, password, hasAuth := r.BasicAuth()
	if hasAuth && user == login && password == pass {
		next(w, r)
	} else {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		response.JSON().CustomResult(http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized).Render(w)
	}
}

func RequestStatMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	startTime := time.Now()
	number := rand.Int()
	log.Debugf("Request #%d url: %s", number, r.URL.String())
	log.Debugf("Request #%d headers: %s", number, r.Header)
	defer func() {
		log.Debugf("Request #%d done in %d ms", number, time.Since(startTime).Nanoseconds()/int64(time.Millisecond))
	}()
	next(w, r)
}
