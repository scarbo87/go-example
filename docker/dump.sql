DROP SCHEMA IF EXISTS `Config`;
CREATE SCHEMA `Config`;
USE `Config`;

CREATE TABLE `Config`.`SystemEvents` (
  `event_type` varchar(32) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`event_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO Config.SystemEvents (event_type, ts) VALUES ("Heartbeat", NOW());

-- создаем событие, для поддержания коннекта демона к mysql binlog и мониторинга отставания
CREATE EVENT IF NOT EXISTS `Heartbeat` ON SCHEDULE EVERY 1 MINUTE STARTS NOW() ON COMPLETION NOT PRESERVE ENABLE DO INSERT INTO Config.SystemEvents (event_type, ts) VALUES ('Heartbeat', NOW()) ON DUPLICATE KEY UPDATE ts = NOW();

DROP SCHEMA IF EXISTS `Example`;
CREATE SCHEMA `Example`;
USE `Example`;

CREATE TABLE `Example`.`Room` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Example`.`User` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `room_id` int(11) unsigned DEFAULT NULL,
  `status` enum('Active','Blocked','Deleted') NOT NULL DEFAULT 'Active',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `User_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `Room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


begin;

insert into `Example`.`Room` (`name`) values ('Room 1');
SET @room1 = LAST_INSERT_ID();
insert into `Example`.`Room` (`name`) values ('Room 2');
SET @room2 = LAST_INSERT_ID();
insert into `Example`.`Room` (`name`, `status`) values ('Room 3', 'Deleted');
SET @room3 = LAST_INSERT_ID();
insert into `Example`.`Room` (`name`) values ('Room 4');
SET @room4 = LAST_INSERT_ID();

insert into `Example`.`User` (first_name, last_name, email, room_id) values ('Дядя', 'Степа', 'd.stepa@example.com', null);
insert into `Example`.`User` (first_name, last_name, email, room_id) values ('Петя', null, null, @room1);
insert into `Example`.`User` (first_name, last_name, email, room_id, `status`) values ('Вася', null, null, @room1, 'Blocked');
insert into `Example`.`User` (first_name, last_name, email, room_id) values (null, 'Петров', 'petrov@example.com', @room3);

commit;

-- генерим тестовые данные
DELIMITER $$
CREATE PROCEDURE insert_test_data()
BEGIN
  DECLARE i INT DEFAULT 1;
  WHILE i < 100000 DO
    INSERT INTO `Example`.`User` (`first_name`, `last_name`, `email`, `room_id`, `status`)
    SELECT `first_name`, `last_name`, null, `room_id`, `status`
    FROM `Example`.`User`
    WHERE id = i % 4;
    SET i = i + 1;
  END WHILE;
END$$
DELIMITER ;
CALL insert_test_data();
DROP PROCEDURE insert_test_data;