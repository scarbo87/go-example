#!/bin/bash

echo "=============== Compiling go-example..."
cd /go/src/go-example
cp ./config/config.yml.dist ./config/config.yml
dep ensure
go build -o ./bin/main ./src/main

echo "=============== Waiting for db set up..."
/usr/local/bin/wait-for-it.sh mariadb:3306 -t 0

echo "=============== Starting go-example..."
./bin/main
